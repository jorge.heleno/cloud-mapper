#!/usr/bin/env python3
import argparse



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
		description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--region', help='Your Google Cloud project ID or aws region.', default="all")
    parser.add_argument(
        '--zone',
        default='all',
        help='Compute Engine zone to use.')
    parser.add_argument('--provider', help='Cloud provider to use. Either aws or gcp.',
            default='all', choices=["gcp","aws","all"])
    args = parser.parse_args()
    #print(get_public_fw(compute, "staging-158815"))
    #print(get_public_fw_tags(compute, "staging-158815"))
    if args.provider == "gcp":
        import gcp
        if args.region == "all":
            gcp.print_rules()
        else:
            gcp.print_rules(args.region, args.zone)
    elif args.provider == "aws":
        import aws
        if args.region == "all":
            aws.print_rules()
        else:
            aws.print_rules(args.region)
    elif args.provider == "all":
        import aws
        import gcp
        aws.print_rules(args.region)
        gcp.print_rules(args.region, args.zone)
    else:
        parser.print_help()
