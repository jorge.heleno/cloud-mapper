#!/usr/bin/env python3

import boto3
from botocore.exceptions import ClientError


ec2 = boto3.resource('ec2')


def get_instances(region):
    ec2 = boto3.resource('ec2', region)
    return ec2.instances.all()

def get_instance(region, id):
    ec2 = boto3.resource('ec2', region)
    return ec2.Instance(id=id)

def get_securitygroups(region):
    ec2 = boto3.resource('ec2', region)
    return ec2.security_groups.all()



def get_source(region, instance, sg):
    ec2 = boto3.resource('ec2', region)
    inbound = []
    return sg

def get_ipranges(rangelist):
    if not rangelist:
        return []
    else:
        return list(rangelist.values())


def print_rule(instance, sg):
    instancename = ""
    for tag in instance.tags:
        if tag['Key'] == "Name":
            instancename = tag['Value']
    try:
        fromport=str(sg['FromPort'])
    except KeyError:
        fromport="*"
    fromport=str(sg['IpProtocol'] if sg['IpProtocol'] != "-1" else "*")+"/"+fromport
    print("{0} <--- {1} {2}".format("private("+instancename+")" if instance.public_ip_address is None else instance.public_ip_address\
            ,str(fromport), ",".join([str(get_ipranges(i)) for i in sg['IpRanges']])))

def is_rule_applicable_in_instance(instance, rule):
#    print("Parsing {0}".format(rule.tags))
    if rule.tags is None:
#        print("NOPE")
        return False
    for i in rule.tags:
        if i in instance.tags:
            return True
    return False

def get_rules_for_instance(instance, sgs):
    rules = []
    for i in sgs:
        if is_rule_applicable_in_instance(instance, i) :
            rules = rules + i.ip_permissions
    return rules

def print_rules():
    for r in boto3.session.Session().get_available_regions('ec2'):
        try:
            print_rules(r)
        except ClientError as e:
            print("Can't list ec2 in {0}".format(r))
            continue

def print_rules(region="eu-west-1"):

    sgs = get_securitygroups(region)
    for i in get_instances(region):
        for j in get_rules_for_instance(i, sgs):
        #           print("Printing rule {0}".format(j))
            print_rule(i,j)
        
