#!/usr/bin/env python3
import googleapiclient.discovery

def get_public_machines(compute, project, zone):
    public_tags = get_public_fw_tags(compute, project)
    instances = compute.instances().list(project=project, zone=zone).execute()
    public_machines = []
    for instance in instances['items']:
        try:
            tags = instance['tags']['items']
        except KeyError:
            continue
        for tag in tags: 
            if tag in public_tags:
                public_machines += [instance]
    return public_machines


def get_public_fw_tags(compute, project):
    public_fw_rules = get_public_fw(compute,project)
    public_tags=[]
    for rule in public_fw_rules:
#        print(rule)
        try:
            public_tags += rule['targetTags']
        except KeyError:
            continue
    return public_tags

def get_fw_rule_by_tag(compute, project, tag):
    rule = ""
    fwrules = compute.firewalls().list(project=project).execute()
    for rule in fwrules['items']:
        try:
            if tag in rule['targetTags']:
                return rule
        except KeyError:
            continue
    return ""

def get_public_fw(compute, project):
    public = []
    fwrules = compute.firewalls().list(project=project).execute()
    for rule in fwrules['items']:
        if "0.0.0.0/0" in rule['sourceRanges']:
            ports=""
            public += [rule]
            for i in rule['allowed']:
                try:
                    ports+="{0}:{1}".format(i['IPProtocol'], i['ports'])
                except KeyError:
                    ports+="{0}".format(i['IPProtocol'])
 #           print("Rule {0} is public with rules {1}".format(rule['name'], ports))
    return public

def print_rules():
    print("Unsupported operation for gcp. Please set project_id")

def print_rules(project_id, zone="all"):
    compute = googleapiclient.discovery.build('compute', 'v1')
    public_machines=[]
    if zone == "all":
        for zone in compute.zones().list(project=project_id).execute()['items']:
            #print(zone)
            try:
                public_machines += get_public_machines(compute, project_id, zone['name'])
            except KeyError:
                continue
    else:
        public_machines = get_public_machines(compute, project_id, zone)
    for i in public_machines:
        try:
             #print(i)
             i['tags']['items']
        except KeyError:
            continue
        for tag in i['tags']['items']:
            active_rules = ""
            rule = get_fw_rule_by_tag(compute, project_id, tag)
            if rule == "":
                continue
            #print(rule)
            for j in rule['allowed']:
                #print(rule)
                try:
                    active_rules += " {0}:{1}".format(j['IPProtocol'],j['ports'])
                except KeyError:
                    active_rules += " {0}".format(j['IPProtocol'])
                print("{1} <-- {0}   : {2}".format("".join(s for s in rule['sourceRanges']), i['name'], active_rules))
